///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file fish.hpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "animal.hpp"

namespace animalfarm {

class Fish : public Animal {
public:
	enum Color scaleColor;
	float favoriteTemp;
	
	virtual const std::string speak();
	void printInfo();
};

} // namespace animalfarm

