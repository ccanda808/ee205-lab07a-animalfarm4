///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {
	
Nunu::Nunu( bool newIsNative, enum Color newColor, enum Gender newGender ) {
	gender = newGender;
	species = "Fistularia chinensis";
	scaleColor = newColor;
	favoriteTemp = 80.6;
	isNative = newIsNative;
}


void Nunu::printInfo() {
	cout << "Nunu" << endl;
	cout << "   Is native = [" << std::boolalpha << isNative << "]" << endl;
	Fish::printInfo();
}

} // namespace animalfarm
