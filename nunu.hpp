///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file nunu.hpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "fish.hpp"

namespace animalfarm {

class Nunu : public Fish {
public:
	Nunu( bool newIsNative, enum Color newColor, enum Gender newGender );

	bool isNative;
	
	void printInfo();
};

} // namespace animalfarm

