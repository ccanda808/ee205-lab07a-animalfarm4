///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.hpp
/// @version 2.0
///
/// Node class
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#pragma once

using namespace std;

namespace animalfarm{

class Node{
   protected:
      Node* next = nullptr;
      friend class SingleLinkedList;
};

} //namespace animalfarm
