###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 07a - Animal Farm 4
#
# @file    Makefile
# @version 1.0
#
# @author Cameron Canda <ccanda@hawaii.edu>
# @brief  Lab 07a - Animal Farm4 - EE 205 - Spr 2021
# @date   31_Mar_2021
###############################################################################

all: main the

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp
	g++ -c mammal.cpp

bird.o: bird.hpp bird.cpp
	g++ -c bird.cpp

fish.o: fish.hpp fish.cpp
	g++ -c fish.cpp
	
nunu.o: nunu.hpp nunu.cpp
	g++ -c nunu.cpp
	
aku.o: aku.hpp aku.cpp
	g++ -c aku.cpp
	
nene.o: nene.hpp nene.cpp
	g++ -c nene.cpp
	
palila.o: palila.hpp palila.cpp
	g++ -c palila.cpp
	
dog.o: dog.cpp dog.hpp
	g++ -c dog.cpp
	
cat.o: cat.cpp cat.hpp
	g++ -c cat.cpp
	
animalFactory.o: animalFactory.cpp *.hpp
	g++ -c animalFactory.cpp

node.o: node.hpp node.cpp
	g++ -c node.cpp

list.o: list.hpp list.cpp
	g++ -c list.cpp

main: main.cpp *.hpp main.o animal.o animalFactory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o
	g++ -o main main.o animal.o animalFactory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o

the:  the.cpp *.hpp main.o animalFactory.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o
	g++ -o the the.cpp animal.o animalFactory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o
	
clean:
	rm -f *.o main the
