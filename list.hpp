///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.hpp
/// @version 2.0
///
/// List class
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "node.hpp"
#pragma once

using namespace std;

namespace animalfarm{

class SingleLinkedList{
   protected:
      Node* head = nullptr;
   public:
      const bool empty() const;
      void push_front( Node* newNode );
      Node* pop_front();
      Node* get_first() const;
      Node* get_next( const Node* currentNode ) const;
      unsigned int size() const;
};

} //namespace animalfarm
