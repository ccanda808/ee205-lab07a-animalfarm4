///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {
	
Palila::Palila( string newWhereFound, enum Color newColor, enum Gender newGender ) {
	gender = newGender;
	species = "Loxioides bailleui";
	featherColor = newColor;
	isMigratory = false;
	whereFound = newWhereFound;
}


void Palila::printInfo() {
	cout << "Palila" << endl;
	cout << "   Where Found = [" << whereFound << "]" << endl;
	Bird::printInfo();
}

} // namespace animalfarm
