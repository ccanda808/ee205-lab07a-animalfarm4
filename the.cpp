///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file test.cpp
/// @version 2.0
///
/// Exports data about all animals
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "animalFactory.hpp"
#include "animal.hpp"
#include "cat.hpp"
#include "node.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	cout << "the" << endl;
	
	for( auto i = 0 ; i < 10 ; i++ ) {
		//cout << Animal::genderName( Animal::getRandomGender() ) << endl;
		//cout << Animal::colorName( Animal::getRandomColor() ) << endl;
		//cout << boolalpha << Animal::getRandomBool() << endl;
		//cout << Animal::getRandomWeight( 4.0, 8.0 ) << endl;
		//cout << Animal::getRandomName() << endl;
		//Animal* animal = AnimalFactory::getRandomAnimal();
		//cout << animal->speak() << endl; 
      Node node; // Instantiate a node
      SingleLinkedList list; // Instantiate a SingleLinkedList
   }
	
	return 0;
}
