///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file palila.hpp
/// @version 1.0
///
/// Exports data about all palila
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "bird.hpp"

namespace animalfarm {

class Palila : public Bird {
public:
	Palila( std::string newWhereFound, enum Color newColor, enum Gender newGender );

	std::string whereFound;
	
	void printInfo();
};

} // namespace animalfarm

