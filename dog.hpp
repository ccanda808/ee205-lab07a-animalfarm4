///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file dog.hpp
/// @version 1.0
///
/// Exports data about all dogs
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "mammal.hpp"

namespace animalfarm {

class Dog : public Mammal {
public:
	std::string name;
	
	Dog( std::string newName, enum Color newColor, enum Gender newGender );
	
	const std::string speak();

	void printInfo();
};

} // namespace animalfarm
