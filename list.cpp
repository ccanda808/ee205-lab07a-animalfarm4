///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 2.0
///
/// Exports data about list
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "list.hpp"

using namespace std;
using namespace animalfarm;

// return true if the list is empty
// false if there's anything in the list
const bool SingleLinkedList::empty() const{
   if(head == nullptr) return true;
   else return false;
}

// Add newNode to the front of the list
void SingleLinkedList::push_front( Node* newNode ){
   newNode -> next = head;
   head = newNode;
}

// Remove a node from the front of the list
// Return nullptr if list is empty
Node* SingleLinkedList::pop_front(){
   if(head == nullptr) return nullptr;
   Node* temp = head;
   head = head -> next;
   return temp;
}

// Return first node from list w/out making changes
Node* SingleLinkedList::get_first() const{
   if(head == nullptr) return nullptr;
   else return head;
}

// Return the node immediately following currentNode
Node* SingleLinkedList::get_next( const Node* currentNode ) const{
   if(currentNode -> next == nullptr) return nullptr;
   else return currentNode -> next;
}

// Return the number of nodes in the list
unsigned int SingleLinkedList::size() const{
   unsigned int n;
   Node* currentNode = head;
   while(currentNode != nullptr){
      currentNode = currentNode -> next;
      n++;
   }
   return n;
}
