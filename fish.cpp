///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file fish.cpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream> 

#include "fish.hpp"

using namespace std;

namespace animalfarm {
	
void Fish::printInfo() {
	Animal::printInfo();
	cout << "   Scale Color = [" << colorName( scaleColor ) << "]" << endl;
	cout << "   Favorite Temprature = [" << favoriteTemp << "]" << endl;
}


const string Fish::speak() {
	return string( "Bubble bubble" );
}


} // namespace animalfarm
