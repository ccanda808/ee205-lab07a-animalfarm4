///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file bird.cpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream> 

#include "bird.hpp"

using namespace std;

namespace animalfarm {
	
void Bird::printInfo() {
	Animal::printInfo();
	cout << "   Feather Color = [" << colorName( featherColor ) << "]" << endl;
	cout << "   Is Migratory = [" << std::boolalpha <<isMigratory << "]" << endl;
}


const string Bird::speak() {
	return string( "Tweet" );
}

} // namespace animalfarm
