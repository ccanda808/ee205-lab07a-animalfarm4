///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {
	
Nene::Nene( string newTagID, enum Color newColor, enum Gender newGender ) {
	gender = newGender;
	species = "Branta sandvicensis";
	featherColor = newColor;
	isMigratory = true;
	tagID = newTagID;
}


const string Nene::speak() {
	return string( "Nay, nay" );
}


void Nene::printInfo() {
	cout << "Nene" << endl;
	cout << "   Tag ID = [" << tagID << "]" << endl;
	Bird::printInfo();
}


} // namespace animalfarm
