///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file dog.cpp
/// @version 1.0
///
/// Exports data about all dogs
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "dog.hpp"

using namespace std;

namespace animalfarm {
	
Dog::Dog( string newName, enum Color newColor, enum Gender newGender ) {
	gender = newGender;
	species = "Canis lupus";
	hairColor = newColor;
	gestationPeriod = 64;
	name = newName;
}


const string Dog::speak() {
	return string( "Woof" );
}


void Dog::printInfo() {
	cout << "Dog Name = [" << name << "]" << endl;
	Mammal::printInfo();
}

} // namespace animalfarm
