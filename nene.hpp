///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file nene.hpp
/// @version 1.0
///
/// Exports data about all nene
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "bird.hpp"

namespace animalfarm {

class Nene : public Bird {
public:
	Nene( std::string tagID, enum Color newColor, enum Gender newGender );

	std::string tagID;

	virtual const std::string speak();	
	void printInfo();
};

} // namespace animalfarm

